import contextlib
from vmrepl.model import *
import pytest
import json

@contextlib.contextmanager
def does_not_raise():
    yield

@pytest.mark.parametrize(
    "code, expectation", [
        ("14", does_not_raise()),
        ("47", does_not_raise()),
        ("03", pytest.raises(ValueError)),
        ("74", pytest.raises(ValueError)),
        ("a1", pytest.raises(ValueError)),
        ("2c", pytest.raises(ValueError))
    ]
)
def test_valid_keycode_construction(code, expectation):
    with expectation:
        KeyCode(code)

@pytest.mark.parametrize(
    "value, expectation", [
        (0.05, pytest.raises(ValueError)),
        (0.10, pytest.raises(ValueError)),
        (0.20, does_not_raise()),
        (0.50, does_not_raise()),
        (1.0, does_not_raise()),
        (2.0, does_not_raise()),
        (5.0, does_not_raise()),
        (10.0, does_not_raise())
    ]
)
def test_valid_currencies(value, expectation):
    with expectation:
        Currency(value)


def test_to_json():
    vm = VendingMachine(
        { KeyCode("12"): Merchandise("Item", 3.50, 2) },
        { Currency(0.2): 10 }
    )
    jsonstr = json.dumps(vm, default=lambda o: o.to_jsonobj())
    print(jsonstr)
    obj = json.loads(jsonstr)

    assert obj == {
        "merch_map": {
            "12": { "name": "Item", "price": 3.50, "stock": 2 },
        },
        "money_reserve": { "0.2": 10 }
    }

def test_from_json():
    vm = VendingMachine.from_jsonobj({
        "merch_map": {
            "12": { "name": "Item", "price": 3.50, "stock": 2 },
        },
        "money_reserve": { "0.2": 10 }
    })
    expected = VendingMachine(
        { KeyCode("12"): Merchandise("Item", 3.50, 2) },
        { Currency(0.2): 10 }
    )
    assert vm.merch_map == expected.merch_map
    assert vm.money_reserve == expected.money_reserve

def test_vending():
    vm = VendingMachine(
        { KeyCode("12"): Merchandise("Item", 0.5, 2) },
        { Currency(2.0): 10, Currency(0.5): 10 },
    )
    vm.insert_money(Currency(5.0))
    vm.insert_money(Currency(0.2))
    item, change = vm.buy(KeyCode("12"))

    assert item == "Item"
    assert change == {
        Currency(2.0): 2,
        Currency(0.5): 1,
        Currency(0.2): 1
    }

def test_raises_arith_exception_when_cannot_split_change():
    vm = VendingMachine(
        { KeyCode("12"): Merchandise("Item", 0.5, 2) },
        { Currency(2.0): 10 },
    )
    vm.insert_money(Currency(5.0))
    vm.insert_money(Currency(0.2))

    with pytest.raises(ArithmeticError):
        item, change = vm.buy(KeyCode("12"))
