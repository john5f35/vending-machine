from dataclasses import dataclass
import re
from enum import Enum
from typing import *
import math
import json

@dataclass
class Merchandise:
    name: str
    price: float
    stock: int

    def to_jsonobj(self):
        return {
            'name': self.name,
            'price': self.price,
            'stock': self.stock
        }

    @staticmethod
    def from_jsonobj(obj: dict):
        return Merchandise(obj['name'], obj['price'], obj['stock'])



class KeyCode(str):
    regex = r'[1-6][1-9]'
    def __new__(cls, string):
        if not re.match(KeyCode.regex, string):
            raise ValueError("Invalid KeyCode format.")
        return super().__new__(cls, string)

class Currency(float):
    accepted_currencies = set([0.20, 0.50, 1.0, 2.0, 5.0, 10.0])
    def __new__(cls, value):
        if not value in Currency.accepted_currencies:
            raise ValueError(f"Currency of value {value} is not accepted")
        return super().__new__(cls, value)

class VendingMachine:
    def __init__(self, merchandise_map: Dict[KeyCode, Merchandise] = {},
                       money_reserve: Dict[Currency, int] = {}):
        self.merch_map = merchandise_map
        self.money_reserve = money_reserve

        self.session_money_input_total = 0.0

    def insert_money(self, amount: Currency):
        self.session_money_input_total += amount
        if amount not in self.money_reserve:    # Bug 1
            self.money_reserve[amount] = 0

    def buy(self, keycode: KeyCode) -> Tuple[str, List[Currency]]:
        merch = self.merch_map.get(keycode, None)
        if merch is None or merch.stock <= 0:
            raise KeyError(f"Nothing at slot {keycode}")
        if self.session_money_input_total < merch.price:
            raise ValueError(f"Insufficient fund!")

        change = self.split_change(self.session_money_input_total - merch.price)
        merch.stock -= 1
        self.session_money_input_total -= merch.price

        return (merch.name, change)

    def abort_session(self) -> Dict[Currency, int]:
        change = self.split_change(self.session_money_input_total)
        self.session_money_input_total = 0.0
        return change

    def split_change(self, change_amount: float) -> Dict[Currency, int]:
        change = {}
        shadow_reserve = self.money_reserve.copy()

        for currency in sorted(shadow_reserve.keys(), reverse=True):
            count = int(change_amount / currency)
            if count > 0 and shadow_reserve[currency] > 0:
                change[currency] = count
                shadow_reserve[currency] -= count
                change_amount -= currency * change[currency]

        if math.isclose(change_amount, 0.0, abs_tol=1e-9):        # Bug 2 floating point arithmatic error
            self.money_reserve = shadow_reserve
            return change

        raise ArithmeticError(f"Cannot split change {change_amount} with money storage.")


    def to_jsonobj(self):
        return {
            'merch_map': self.merch_map,
            'money_reserve': self.money_reserve
        }

    @staticmethod
    def from_jsonobj(obj: dict):
        return VendingMachine(
            { KeyCode(kc): Merchandise.from_jsonobj(merch) for kc, merch in obj['merch_map'].items() },
            { Currency(float(c)): count for c, count in obj['money_reserve'].items() }
        )

