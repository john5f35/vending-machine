## Tasks

### 1. Fix broken tests
    Some tests are failing, fix them

### 2. Bug fixes

#### Empty inputs terminates the program

When given empty input, the program exits.

```
(Vending Machine REPL)>>
Invalid KeyCode format.
```

It should be able to withstand empty inputs and keep the session alive.

#### When exiting due to invalid input, money not returned

Money got swallowed when the input was wrong:

```
(Vending Machine REPL)>> $2
Inserted $2.0; total: $2.0
(Vending Machine REPL)>> fdsa
Invalid KeyCode format.
```

### Machine state not saved when exiting with <Ctrl-D>

Ending session with <Ctl-D> doesn't save machine state to JSON.

```
(Vending Machine REPL)>> (<Ctl-D>)

Aborted!
```

The machine state should be saved always, except with `--no-save` flag.

### Negative count in machine state

With the following machine state:

```
$ cat broken-machine.json
{"merch_map": {"11": {"name": "Pencil", "price": 1.0, "stock": 4}, "12": {"name": "Coca-Cola can", "price": 3.5, "stock": 6}, "13": {"name": "Nuts", "price": 3.9, "stock": 8}}, "money_reserve": {"0.2": 1, "0.5": 1, "1.0": 1, "2.0": 1, "5.0": 1}}
```

Buying a $1.0 Pencil with $5.0 note leaves machine money reserve containing negative counts:

```
(Vending Machine REPL)>> $5
Inserted $5.0; total: $5.0
(Vending Machine REPL)>> 11
You bought a 'Pencil'!
Here are your changes:
  Currency    count
----------  -------
         2        2
Saving machine state...
$ cat broken-machine.json
{"merch_map": {"11": {"name": "Pencil", "price": 1.0, "stock": 3}, "12": {"name": "Coca-Cola can", "price": 3.5, "stock": 6}, "13": {"name": "Nuts", "price": 3.9, "stock": 8}}, "money_reserve": {"0.2": 1, "0.5": 1, "1.0": 1, "2.0": -1, "5.0": 2}}
```

### 3. Feature: multi-buy
    Add ability to specify how many items want to buy; as well as buying multiple merchandises in one session.