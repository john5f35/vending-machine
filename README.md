# Toy Vending Machine REPL

## Install

Pre-requisites:
- Python 3.7+ is required
- Python environment and packages are managed through [poetry](https://github.com/python-poetry/poetry)

Clone this repository.

```bash
$ poetry install
$ poetry shell
(.venv) $ vmrepl --help
Usage: vmrepl [OPTIONS] JSON_FILE

Options:
  --help  Show this message and exit.

```

Set up a vending machine state JSON file like the following:

```json
{
    "merch_map": {
        "12": {"name": "Item", "price": 3.5, "stock": 2},
        "23": {"name": "Item2", "price": 5.8, "stock": 3}
    },
    "money_reserve": {"0.2": 14, "5.0": 1}
}
```

Run tests with:

```
$ pytest -v
```
