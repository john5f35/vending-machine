import click
import io, sys
import json
import prompt_toolkit
from tabulate import tabulate
from vmrepl.model import VendingMachine, Currency, KeyCode

HELP_TEXT=f"""
Vending Machine REPL

- Type 'list' to list all available merchandises;
- Type '$<amount>' to insert money (e.g. $0.2 to insert 20 cents)
    Accepted currencies are: {Currency.accepted_currencies}
- Type '<keycode>' to buy desired item;
- Type 'help' to show this help text;
- '<Ctl-D>' to exit
"""
PROMPT="(Vending Machine REPL)>> "

def process_user_input(vm: VendingMachine, user_input: str) -> bool:
    if user_input == 'help':
        click.echo(HELP_TEXT)
        return True
    if user_input == 'list':
        table = [ (kc, merch.name, f"${merch.price}") for kc, merch in vm.merch_map.items() if merch.stock > 0 ]
        click.echo(tabulate(table, headers=('Key Code', 'Merchandise', 'Price')))
        return True
    if user_input.startswith("$"):
        try:
            c = Currency(float(user_input[1:]))
            vm.insert_money(c)
            click.echo(f"Inserted ${c}; total: ${vm.session_money_input_total}")
        except ValueError as e:
            click.echo(str(e))
        return True

    try:
        code = KeyCode(user_input)
        item, change = vm.buy(code)
        click.echo(f"You bought a '{item}'!")
        if len(change) > 0:
            click.echo("Here are your changes:")
            click.echo(tabulate(change.items(), headers=('Currency', 'count')))
    except ArithmeticError as e:
        click.echo(str(e))
        click.echo("Here are your changes:")
        click.echo(tabulate(vm.abort_session().items(), headers=('Currency', 'count')))
    except Exception as e:
        click.echo(str(e))
    return False

@click.command()
@click.option("--save/--no-save", default=True, help="Don't save the machine state back to JSON")
@click.argument("json_file", type=click.Path(exists=True, dir_okay=False, readable=True, writable=True))
def main(json_file: str, save: bool):
    with open(json_file, 'r') as fp:
        vm = VendingMachine.from_jsonobj(json.load(fp))

    click.echo(HELP_TEXT)

    session_alive = True
    while session_alive:
        user_input: str = prompt_toolkit.prompt(PROMPT)
        session_alive = process_user_input(vm, user_input)

    if save:
        click.echo("Saving machine state...")
        with open(json_file, 'w') as fp:
            json.dump(vm, fp, default=lambda o: o.to_jsonobj())


if __name__ == "__main__":
    main()