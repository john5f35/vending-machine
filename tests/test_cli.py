from vmrepl.cli import process_user_input
from vmrepl.model import VendingMachine, Currency, KeyCode, Merchandise

def test_inserting_money_keeps_session_alive():
    vm = VendingMachine()
    assert process_user_input(vm, "$1.0")
    assert process_user_input(vm, "$a")
    assert process_user_input(vm, "$23.53")

def test_buying_item_ends_session_after():
    vm = VendingMachine(
        { KeyCode("12"): Merchandise("Candy", 1.2, 5) }
    )

    process_user_input(vm, "$1.0")
    process_user_input(vm, "$0.5")
    assert not process_user_input(vm, "12")